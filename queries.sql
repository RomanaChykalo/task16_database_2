use labor_sql;
select maker FROM product WHERE model IN ( SELECT model FROM pc );
select maker FROM product WHERE model != ALL ( SELECT model FROM laptop);
select maker FROM product WHERE model = ANY( SELECT model FROM pc);
select maker FROM product WHERE model IN ( SELECT model FROM pc) AND model IN ( SELECT model FROM laptop);
select maker FROM product WHERE model = ALL ( SELECT model FROM laptop) and model = ALL ( SELECT model FROM pc);

select maker from product where exists(select * from pc where model=product.model);
select maker from product where exists(select * from pc where speed>=750 );
select maker from product where exists(select * from pc where speed>=750 ) and exists(select * from laptop where speed>=750 );
select maker from product,printer where exists(select * from pc where model=printer.model) and exists(select * from pc where speed>=750 );
select name, launched, displacement from ships, classes where exists(select * from ships where launched>1922) and exists(select * from classes where bore>35);

select 'average price: ',AVG(Price) from laptop;
select 'pc code: ', code, 'model: ', model, 'pc-speed: ', speed, 'ram: ',ram,'hd: ',hd, 'cd: ', cd, 'price: ', price from pc;
select CONVERT(VARCHAR(10), GETDATE(), 102) from income;

select model, price from printer GROUP BY price desc limit 3;
select product.type, laptop.model, laptop.speed from laptop, product where speed < all(SELECT speed FROM pc) and type='Laptop';
select model, price FROM printer group by model having MAX(price) ;
select product.type, laptop.model, laptop.speed from laptop, product where speed < all(SELECT speed FROM pc) and type='Laptop';
select maker, printer.price from product product join printer on product.model = printer.model where printer.price = (select min(price) from printer where color = 'y');
select maker from product WHERE model = any ( SELECT model FROM pc );
SELECT model, count(model) AS model_num FROM pc GROUP BY model;
select maker, count(model) from product where type = 'PC' group by maker having count(model) >= 2;
select pc.model,avg(hd) from pc, product where pc.model=product.model and product.maker in 
 (select maker from product where type='Printer') group by product.maker;
 
select maker, (select count(model) FROM laptop where model=product.model) AS laptop_num,
(select count(model) FROM pc where model=product.model ) AS pc_num,
(select count(model) FROM printer where model=product.model) AS printer_num from product group by maker;

select maker,(select avg(screen) from laptop group by model having model=product.model) 
AS screen_size from product group by maker;
select maker, avg(screen) from product, laptop where product.model = laptop.model
group by maker;

select maker,(select max(price) from pc where model=product.model) 
AS max_price from product group by maker;

select speed, AVG(price) as avg_price from pc where speed > 600 group by speed;
