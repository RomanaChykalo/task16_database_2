create database if not exists university;
use university;
create table if not exists cities
 (
 id int auto_increment primary key not null,
 name varchar(10) not null
 );
 create table if not exists streets
 (
 id int auto_increment primary key not null,
 name varchar(10) not null
 );
create table if not exists specialities
(
id int auto_increment primary key,
 name varchar(10) not null
);
create table if not exists groups
(
code int auto_increment primary key,
id_specialities int null,
CONSTRAINT fk_groups_specialities
FOREIGN KEY (id_specialities)
REFERENCES specialities (id)
);
create table if not exists teachers
(
id int auto_increment primary key,
surname char(10) NOT NULL,
name char(10) NOT NULL
);
create table if not exists subjects
( 
id int AUTO_INCREMENT PRIMARY KEY ,
name char(10) NOT NULL
);
create table if not exists students
( 
id int AUTO_INCREMENT PRIMARY KEY,
surname char(10) not null,
name char(10) not null,
photo varbinary(20),
autobiography varchar(50),
group_code int,
date_of_birth date not null,
date_of_enter date not null,
city_id int,
street_id int,
subject_id int(10),
rating int default null,
scholarship int default null,
CONSTRAINT FK_students_cities
FOREIGN KEY (city_id)
REFERENCES cities (id)
ON UPDATE cascade ON DELETE SET NULL,
CONSTRAINT FK_students_streets
FOREIGN KEY (street_id)
REFERENCES streets (id)
ON UPDATE cascade ON DELETE SET NULL,
constraint FK_students_groups
FOREIGN KEY (group_code)
REFERENCES groups (code)
ON DELETE SET NULL,
constraint FK_students_subjects1
FOREIGN KEY (subject_id)
REFERENCES subjects (id)
ON DELETE SET NULL ON UPDATE SET NULL
); 

create table if not exists students_subjects
( 
id int AUTO_INCREMENT PRIMARY KEY ,
id_student int,
id_subject int,
CONSTRAINT FK_subjects_students
FOREIGN KEY (id_subject)
REFERENCES subjects (id)
ON UPDATE set null,
CONSTRAINT FK_students_subjects
FOREIGN KEY (id_student)
REFERENCES students (id)
ON UPDATE set null
);
create table if not exists general_estimates
(
id int AUTO_INCREMENT PRIMARY KEY,
id_students_subjects int,
id_teacher int,
id_module_estimates int,
control_type varchar(10),
semester_number int,
CONSTRAINT FK_students_subjects_general_estimate
FOREIGN KEY (id_students_subjects)
REFERENCES students_subjects (id)
ON UPDATE cascade ON delete set null,
CONSTRAINT FK_general_estimates_teacher
FOREIGN KEY (id_teacher)
REFERENCES teachers (id)
ON UPDATE cascade ON delete set null,
CONSTRAINT FK_general_estimates_module_estimates
FOREIGN KEY (id_module_estimates)
REFERENCES module_estimates (id)
ON UPDATE cascade ON delete set null
);
create table if not exists module_estimates
(
id int AUTO_INCREMENT PRIMARY KEY,
first_module_result int,
second_module_result int,
semester_result_estimate int GENERATED ALWAYS AS (first_module_result + second_module_result)
);